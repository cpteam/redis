<?php

namespace CPTeam;

/**
 * @package CPTeam
 */
class Redis
{
	/** @var \Redis */
	private $redis;
	
	private const EXPIRATION = 3600;
	
	/**
	 * @param \Redis $redis
	 */
	public function __construct(\Redis $redis)
	{
		$this->redis = $redis;
	}
	
	public function set(string $key, $value, int $expiration = self::EXPIRATION) : bool
	{
		return $this->redis->set($key, serialize($value), $expiration);
	}
	
	public function get(string $key)
	{
		return unserialize($this->redis->get($key));
	}
}
